" Specify a directory for plugins (for Neovim: ~/.local/share/nvim/plugged)
call plug#begin('~/.local/share/nvim/plugged')

" call plug#begin('/tmp/nvim/plugged')

" Make sure you use single quotes

" Shorthand notation; fetches https://github.com/junegunn/vim-easy-align

Plug 'honza/vim-snippets'
Plug 'scrooloose/nerdtree'
Plug 'kien/ctrlp.vim'
Plug 'easymotion/vim-easymotion'
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline'
Plug 'tpope/vim-markdown'
Plug 'fatih/vim-go', { 'do': ':GoInstallBinaries' }
Plug 'jiangmiao/auto-pairs'
Plug 'neovimhaskell/haskell-vim'
Plug 'ryanoasis/vim-devicons'
Plug 'hashivim/vim-terraform'
Plug 'alx741/vim-stylishask'
Plug 'chriskempson/base16-vim'
Plug 'neoclide/coc.nvim', {'do': 'yarn install --frozen-lockfile'}

" Initialize plugin system
call plug#end()

set encoding=UTF-8


" General
set number
let mapleader=' '
set nowrap
set autoread

" Cogs
syntax on
filetype plugin indent on
syntax enable
set background=dark
highlight Comment cterm=italic
" colorscheme base16-chalk
colorscheme base16-default-dark
set termguicolors

" Buffer ignore
augroup BR
autocmd BufWritePre * :%s/\s\+$//e
augroup END

" Folding
set foldmethod=indent

" WildMenu for filename completion
set wildmode=longest,list,full
" Default settings for all files
setlocal tabstop=2
setlocal shiftwidth=2
setlocal expandtab

" Filetypes
augroup filetype_
autocmd BufNewFile,BufRead *.go setf go
autocmd BufNewFile,BufRead *.sql setf pgsql
augroup END

" Maps
noremap <leader>a =ip
noremap <S-l> gt
noremap <S-h> gT
noremap <leader>q :q<cr>
noremap <leader>w :w<cr>
noremap <leader>wq :wq<cr>

" File OPS
let g:markdown_fenced_languages = ['html', 'python', 'bash=sh', 'go']
let g:markdown_syntax_conceal = 0

" Navigation
" Use JKLH for navigation between split panes
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" More natural splits
set splitbelow
set splitright

" Nerd Tree
map tt :NERDTreeToggle<CR>

" Easy Motion
let g:EasyMotion_do_mapping = 0 " Disable default mappings

" --------------------------------------------------------------------
"
" Jump to anywhere you want with minimal keystrokes, \
" with just one key binding.
" `s{char}{label}`
" nmap s <Plug>(easymotion-overwin-f)
" or
" `s{char}{char}{label}`
" Need one more keystroke, but on average, it may be more comfortable.
"
" --------------------------------------------------------------------
nmap <Leader>s <Plug>(easymotion-overwin-f2)

" Turn on case insensitive feature
let g:EasyMotion_smartcase = 1

" JK motions: Line motions
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)



" Py Mode
" Figure out the system Python for Neovim.
if exists('$VIRTUAL_ENV')
    let g:python_host_prog=$VIRTUAL_ENV .  '/bin/python'
else
    let g:python_host_prog=substitute(system('which -a python3'), '\n', '', 'g')
endif
let g:syntastic_python_checkers=['mypy']
" let g:pymode_python = 'python3'           " Lets you open documentation
let g:pymode_doc = 1                			" Lets you open documentation
let g:pymode_doc_bind = 'K'         		 	" Docs key map
let g:pymode_virtualenv = 1         			" Open virtual env
let g:pymode_virtualenv_path = $VIRTUAL_ENV

let g:pymode_rope = 1
let g:pymode_rope_autoimport = 1
let g:pymode_rope_project_root = '/tmp/.ropeproject'
let g:pymode_rope_regenerate_on_write = 1
let g:pymode_rope_goto_definition_bind = '<C-c>g'
let g:pymode_rope_goto_definition_cmd = 'new'
let g:pymode_rope_rename_bind = '<C-c>rr'               " Rename :D
let g:pymode_rope_rename_module_bind = '<C-c>r1r'       " Rename the module :D
let g:pymode_rope_use_function_bind = '<C-c>ru'
let g:pymode_rope_completion_bind = '<C-.>'
let g:pymode_rope_complete_on_dot = 1
let g:pymode_rope_autoimport_bind = '<C-c>ra'
let g:pymode_lint_on_fly = 1
let g:pymode_syntax = 1
let g:pymode_syntax_slow_sync = 1
let g:pymode_syntax_print_as_function = 1


" --- SQL
let g:sql_type_default = 'pgsql'


" 80 everywhere?
" highlight OverLength ctermbg=red ctermfg=white guibg=#592929
" match OverLength /\%81v.\+/
set colorcolumn=80

map <C-n> :cnext<CR>
map <C-m> :cprevious<CR>
nnoremap <leader>c :cclose<CR>

" Vim goodies?
set incsearch

" Statusline
set statusline+=%#warningmsg#
" set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

" Vim auto-pairs
let g:AutoPairsFlyMode = 0
let g:AutoPairsShortcutBackInsert = '<M-b>'

augroup go_auto
autocmd FileType go nmap <leader>b  <Plug>(go-build)
augroup END
" autocmd FileType go nmap <Leader>i <Plug>(go-info)

let g:go_metalinter_enabled = ['vet', 'golint', 'errcheck', 'deadcode']

" Open go doc in vertical window, horizontal, or tab
augroup vgo
au Filetype go nnoremap <leader>v :vsp <CR>:exe "GoDef" <CR>
au Filetype go nnoremap <leader>s :sp <CR>:exe "GoDef"<CR>
au Filetype go nnoremap <leader>t :tab split <CR>:exe "GoDef"<CR>
au FileType go setl sw=2 ts=2 et
augroup END


let g:go_fmt_command = 'goimports'
let g:go_list_type = 'quickfix'

" vim-go
let g:go_auto_type_info = 1
" let g:go_info_mode = 'guru'
let g:go_auto_sameids = 1
let g:go_def_reuse_buffer = 1
let g:go_highlight_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_generate_tags = 1
let g:go_def_mode='gopls'
let g:go_info_mode='gopls'
" disable vim-go :GoDef short cut (gd)
" this is handled by LanguageClient [LC]
let g:go_def_mapping_enabled = 0


" Folding for JavaScript?
augroup javascript_folding
  au!
  au FileType javascript setlocal foldmethod=syntax
  autocmd FileType javascript setl sw=4 ts=4 et
augroup END

" ---------------------------------------------------------------
" --------------------------- HASKELL ---------------------------
" ---------------------------------------------------------------

" Haskell Vim
let g:haskell_enable_quantification = 1   " to enable highlighting of `forall`
let g:haskell_enable_recursivedo = 1      " to enable highlighting of `mdo` and `rec`
let g:haskell_enable_arrowsyntax = 1      " to enable highlighting of `proc`
let g:haskell_enable_pattern_synonyms = 1 " to enable highlighting of `pattern`
let g:haskell_enable_typeroles = 1        " to enable highlighting of type roles
let g:haskell_enable_static_pointers = 1  " to enable highlighting of `static`
let g:haskell_backpack = 1                " to enable highlighting of backpack keywords

" Some crazy indents
let g:haskell_indent_if = 3
let g:haskell_indent_case = 2
let g:haskell_indent_let = 4
let g:haskell_indent_where = 6
let g:haskell_indent_before_where = 2
let g:haskell_indent_after_bare_where = 2
let g:haskell_indent_do = 3
let g:haskell_indent_in = 1
let g:haskell_indent_guard = 2
let g:haskell_indent_case_alternative = 1

" Stylish-Haskell
let g:stylishask_on_save = 1
augroup Haskell
  au FileType haskell setl sw=2 ts=2 et
augroup END

" Cabal Indentation
let g:cabal_indent_section = 2

" ---------------------------------------------------------------
" ---------------------------------------------------------------
" ---------------------------------------------------------------

" Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0

" Terraform
let g:terraform_align=1
let g:terraform_fold_sections=1
let g:terraform_fmt_on_save=1

" Language Client

augroup yaml_json
autocmd BufNewFile,BufRead *.yaml setf yaml
au FileType yaml setl sw=2 ts=2 et

autocmd BufNewFile,BufRead *.json setf json
au FileType json setl sw=2 ts=2 et
augroup END

" Shell config
augroup Shell_
autocmd BufNewFile,BufRead *.sh setf sh
au FileType sh setl sw=2 ts=2 et
augroup END


" Vim lint
let g:syntastic_vim_checkers = ['vint']

" -------------------------------------------------------------------------------------------------
" coc.nvim default settings
" -------------------------------------------------------------------------------------------------

" if hidden is not set, TextEdit might fail.
set hidden

" Better display for messages
" set cmdheight=1 -- TODO: use default, change later if necessary

" Smaller updatetime for CursorHold & CursorHoldI
set updatetime=300
" don't give |ins-completion-menu| messages.
set shortmess+=c
" always show signcolumns
set signcolumn=yes

" Use tab for trigger completion with characters ahead and navigate.
" Use command ':verbose imap <tab>' to make sure tab is not mapped by other plugin.
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" Use `[c` and `]c` to navigate diagnostics
nmap <silent> [c <Plug>(coc-diagnostic-prev)
nmap <silent> ]c <Plug>(coc-diagnostic-next)


" Remap keys for gotos
nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

" Use U to show documentation in preview window
nnoremap <silent> U :call <SID>show_documentation()<CR>

" Remap for rename current word
nmap <leader>rn <Plug>(coc-rename)

" Remap for format selected region
vmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

" TODO: space and leader reconfig - sanity please
" Show all diagnostics
" nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions
" nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands
" nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document
" nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols
" nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
" nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
"nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list
" nnoremap <silent> <space>p  :<C-u>CocListResume<CR>

" COC error colors

" TODO: Coc extension management
" List of extensions installed
" coc-python
" coc-json
" coc-yaml
" coc-docker
